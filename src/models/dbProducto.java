/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Jonha
 */
public class dbProducto extends DbManejador implements dbPersistencia{

    @Override
    public void insertar(Object Objeto) throws Exception {
        
        Productos pro = new Productos();
        pro = (Productos) Objeto;
        
        String consulta = "insert into "
                + "productos(codigo,nombre,fecha,precio,estatus)values(?,?,?,?,?)";
        
       
        if(this.conectar()){
            try{
                System.err.println("Se conecto");
                this.sqlConsulta = this.conexion.prepareStatement(consulta);
                //Asignar valores
                this.sqlConsulta.setString(1, pro.getCode());
                this.sqlConsulta.setString(2, pro.getNombre());
                this.sqlConsulta.setString(3, pro.getFecha());
                this.sqlConsulta.setFloat(4, pro.getPre());
                this.sqlConsulta.setInt(5, pro.getStatus());
                
                this.sqlConsulta.executeUpdate();
                this.desconectar();
            }catch(SQLException e){
                System.err.println("Surgio un error al insertar " + e.getMessage());
            }
        }
        
        else{
            System.out.println("No fue posible conectarse");
        }
        
    }

    @Override
    public void actualizar(Object Objeto) throws Exception {
        
        Productos pro = new Productos();
        pro = (Productos) Objeto;
        
        String consulta = "update productos set nombre = ?, precio = ?, " + "fecha = ?, where codigo = ?";
        
        if(this.conectar()){
            try{
                System.err.println("Se conecto");
                this.sqlConsulta = this.conexion.prepareStatement(consulta);
                //Asignar valores
                this.sqlConsulta.setString(1, pro.getCode());
                this.sqlConsulta.setString(2, pro.getNombre());
                this.sqlConsulta.setString(3, pro.getFecha());
                this.sqlConsulta.setFloat(4, pro.getPre());
                
                this.sqlConsulta.executeUpdate();
                this.desconectar();
            }catch(SQLException e){
                System.err.println("Surgio un error al insertar " + e.getMessage());
            }
        }
        
        else{
            System.out.println("No fue posible conectarse");
        }
    }

    @Override
    public void habilitar(Object Objeto) throws Exception {
        Productos pro = new Productos();
        pro = (Productos) Objeto;
        
        String consulta = "UPDATE productos SET estatus = 1 WHERE codigo = ?";
        
        if(this.conectar()) {
            try {
                System.out.println("Se conectó");
                this.sqlConsulta = this.conexion.prepareStatement(consulta);
                
                // Se asignan los valores
                this.sqlConsulta.setString(1, pro.getCode());
            
                this.sqlConsulta.executeUpdate();
                this.desconectar();
            }
            catch(Exception e) {
                System.err.println("Error: " + e.getMessage());
            }
        }
        else {
            System.err.println("No se logró conectar");
        }
    }

    @Override
    public void deshabilitar(Object Objeto) throws Exception {
        Productos pro = new Productos();
        pro = (Productos) Objeto;
        
        String consulta = "UPDATE productos SET estatus = 0 WHERE codigo = ?";
        
        if(this.conectar()) {
            try {
                System.out.println("Se conectó");
                this.sqlConsulta = this.conexion.prepareStatement(consulta);
                
                // Se asignan los valores
                this.sqlConsulta.setString(1, pro.getCode());
            
                this.sqlConsulta.executeUpdate();
                this.desconectar();
            }
            catch(Exception e) {
                System.err.println("Error" + e.getMessage());
            }
        }
        else {
            System.err.println("No se logró conectar");
        }
        
    }

    @Override
    public boolean isExists(String id) throws Exception {
        Productos pro = new Productos();
        
        if(this.conectar()) {
            String consulta = "SELECT * FROM productos WHERE codigo = ?";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            
            // Asignar valores
            this.sqlConsulta.setString(1, id);
            
            // Hacer la consulta
            this.registros = this.sqlConsulta.executeQuery();
            
            // Se checa si el registro del código existe
            if(this.registros.next()) {
                if (this.registros.getString("codigo") != null)
                    return true;
                else
                    return false;
            }
        }
        this.desconectar();
        return false;
    }

    @Override
    public ArrayList lista() throws Exception {
        ArrayList<Productos> lista = new ArrayList<Productos>();
        Productos pro;
        
        if(this.conectar()){
            String consulta = "Select * from productos ORDER BY codigo";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.registros = this.sqlConsulta.executeQuery();
            
            while(this.registros.next()){
                pro = new Productos();
                pro.setIdPro(this.registros.getInt("idProducto"));
                pro.setCode(this.registros.getString("codigo"));
                pro.setPre(this.registros.getFloat("precio"));
                pro.setFecha(this.registros.getString("fecha"));
                pro.setStatus(this.registros.getInt("estatus"));
                pro.setNombre(this.registros.getString("nombre"));
                lista.add(pro);
            }
        }
        this.desconectar();
        return lista;
    }

    @Override
    public ArrayList listar(String crit) throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Object buscar(int id) throws Exception {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Object buscar(String code, int estatus) throws Exception {
        Productos pro = new Productos();
        
        if(this.conectar()){
            String consulta = "Select * from productos where codigo = ? AND estatus = ?";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            
            this.sqlConsulta.setString(1, code);
            this.sqlConsulta.setInt(2, estatus);
            
            if(estatus == 2){
                consulta = "SELECT * FROM productos WHERE codigo = ?";
                this.sqlConsulta = this.conexion.prepareStatement(consulta);
                this.sqlConsulta.setString(1, code);
            }
            this.registros = this.sqlConsulta.executeQuery();
            
            if(this.registros.next()){
                pro.setIdPro(this.registros.getInt("idProducto"));
                pro.setCode(this.registros.getString("codigo"));
                pro.setPre(this.registros.getFloat("precio"));
                pro.setFecha(this.registros.getString("fecha"));
                pro.setStatus(this.registros.getInt("estatus"));
                pro.setNombre(this.registros.getString("nombre"));
            }
        }
        this.desconectar();
        return pro;
    }
    
}
