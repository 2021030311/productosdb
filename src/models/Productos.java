/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author Jonha
 */
public class Productos {
    private int idPro;
    private String code;
    private String nombre;
    private float pre;
    private String fecha;
    private int status;

    public Productos(int idPro, String code, String nombre, float pre, String fecha, int status) {
        this.idPro = idPro;
        this.code = code;
        this.nombre = nombre;
        this.pre = pre;
        this.fecha = fecha;
        this.status = status;
    }

    public Productos() {
    }

    public int getIdPro() {
        return idPro;
    }

    public void setIdPro(int idPro) {
        this.idPro = idPro;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getPre() {
        return pre;
    }

    public void setPre(float pre) {
        this.pre = pre;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
    
    
}
