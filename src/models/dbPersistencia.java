/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package models;
import java.util.ArrayList;
/**
 *
 * @author Jonha
 */
public interface dbPersistencia {
    public void insertar(Object Objeto) throws Exception;
    public void actualizar(Object Objeto) throws Exception;
    public void habilitar(Object Objeto) throws Exception;
    public void deshabilitar(Object Objeto) throws Exception;
    
    public boolean isExists(String id) throws Exception;
    public ArrayList lista() throws Exception;
    public ArrayList listar(String crit) throws Exception;
    
    public Object buscar(int id) throws Exception;
    public Object buscar(String code, int estatus) throws Exception;
    
}
