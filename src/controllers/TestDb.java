
package controllers;
import java.util.ArrayList;
import models.Productos;
import models.dbProducto;
import views.dlgProductos;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class TestDb implements ActionListener {

    /*public static void main(String[] args) {
        Productos pro = new Productos();
        dbProducto db = new dbProducto();
        
        pro.setCode("001");
        pro.setFecha("2023-6-22");
        pro.setNombre("Avena");
        pro.setPre(34.6f);
        pro.setStatus(0);
        
        try{
            db.insertar(pro);
            System.out.println("Se agrego con exito");
        }catch(Exception e){
            System.err.println("Surgio un error" + e.getMessage());
        }
    }*/
    private Productos pro = new Productos();
    private dbProducto db = new dbProducto();
    private dlgProductos vista = new dlgProductos(new JFrame(), true);
    private static int sw = 0;
    
    public TestDb(Productos pro, dbProducto db, dlgProductos vista) {
        this.pro = pro;
        this.db = db;
        this.vista = vista;
        
        vista.btnNuevo.addActionListener(this);
        vista.btnBuscar.addActionListener(this);
        vista.btnSearch2.addActionListener(this);
        vista.btnDeshabilitar.addActionListener(this);
        vista.btnHabilitar.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        
        vista.btnLimpiar.addActionListener(this);
        vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        
        ActualizarTabla(vista.jtbActivos, 1);
        ActualizarTabla(vista.jtbNoActivos, 0);
    }
    
    public void iniciarVista() {
        this.vista.setSize(600, 530);
        this.vista.setVisible(true);
        this.vista.setTitle("== Productos ==");
    }
    
    public void limpiar() {
        vista.txtCodigo.setText("");
        vista.jDateChooser1.setDate(null);
        vista.txtID.setText("");
        vista.txtNombre.setText("");
        vista.lname.setText("");
        vista.txtPrecio.setText("");
    }
    
    public void limpiar2() {
        vista.jDateChooser1.setDate(null);
        vista.txtID.setText("");
        vista.txtNombre.setText("");
        vista.lname.setText("");
        vista.txtPrecio.setText("");
    }
    
    public void ingresar() {
        try {
            pro.setCode(vista.txtCodigo.getText());
            pro.setNombre(vista.txtNombre.getText());
            pro.setPre(Float.parseFloat(vista.txtPrecio.getText()));
            pro.setStatus(1);
            LocalDate selectedDate = vista.jDateChooser1.getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            String formattedDate = selectedDate.format(formatter);
            this.pro.setFecha(formattedDate);
        }
        catch(Exception e) {
            JOptionPane.showMessageDialog(vista, "Error: " + e.getMessage());
            return;
        }
    }
    
    public void ActualizarTabla(JTable table, int status) {
        try {
            ArrayList<Productos> productosList = db.lista();

            String[] columnNames = {"ID", "Código", "Nombre", "Precio", "Fecha", "Estatus"};
            DefaultTableModel tableModel = new DefaultTableModel(columnNames, 0);

            for (Productos producto : productosList) {
                Object[] rowData = {
                    producto.getIdPro(),
                    producto.getCode(),
                    producto.getNombre(),
                    producto.getPre(),
                    producto.getFecha(),
                    producto.getStatus()
                };
                if((int)rowData[5] == status)
                    tableModel.addRow(rowData);
            }

            table.setModel(tableModel);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == vista.btnNuevo)
        {
            vista.txtCodigo.setEnabled(true);
            vista.txtNombre.setEnabled(true);
            vista.txtPrecio.setEnabled(true);
            vista.jDateChooser1.setEnabled(true);
            vista.btnGuardar.setEnabled(true);
            vista.btnDeshabilitar.setEnabled(true);
            vista.btnLimpiar.setEnabled(true);
            vista.btnCancelar.setEnabled(true);
            limpiar2();
            sw = 1;
        }
        
        if(e.getSource() == vista.btnGuardar) {
            
            try {
                if(Integer.parseInt(vista.txtCodigo.getText()) <= 0){
                    throw new IllegalArgumentException("No puede haber un código negativo o cero");
                }
                
                if(db.isExists(vista.txtCodigo.getText()) == true && sw != 2)
                {
                    JOptionPane.showMessageDialog(vista, "Este codigo ya existe");
                    return;
                }
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(vista, "Error: " + ex.getMessage());
                return;
            }
            //ingresar();    
            try {
                pro.setCode(vista.txtCodigo.getText());
                pro.setNombre(vista.txtNombre.getText());
                pro.setPre(Float.parseFloat(vista.txtPrecio.getText()));
                pro.setStatus(1);
                LocalDate selectedDate = vista.jDateChooser1.getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                String formattedDate = selectedDate.format(formatter);
                this.pro.setFecha(formattedDate);
                
                if(sw == 1)
                {
                    db.insertar(this.pro);
                    JOptionPane.showMessageDialog(vista, "Se agrego con exito");
                }
                else
                {
                    db.actualizar(this.pro);
                    JOptionPane.showMessageDialog(vista, "Se actualizo con exito");
                }
            }
            catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(vista, "ERROR: " + ex.getMessage());
                return;
            
            }catch(IllegalArgumentException ex2) {
                JOptionPane.showMessageDialog(vista, "SURGIO UN ERROR: " + ex2.getMessage());
                return;
            }
            catch(Exception ex) {
                JOptionPane.showMessageDialog(vista, "SURGIO UN ERROR: " + ex.getMessage());
                return;
            }
            limpiar();
            vista.txtCodigo.setEnabled(true);
            vista.txtNombre.setEnabled(false);
            vista.txtPrecio.setEnabled(false);
            vista.jDateChooser1.setEnabled(false);
            vista.btnGuardar.setEnabled(false);
            vista.btnDeshabilitar.setEnabled(false);
        }
        
        if(e.getSource() == vista.btnHabilitar) {
            try {
                this.pro.setStatus(1);
                db.habilitar(this.pro);
                JOptionPane.showMessageDialog(vista, "El producto fue activado con exito");
            }
            catch(Exception ex) {
                JOptionPane.showMessageDialog(vista, "SURGIO UN ERROR: " + ex.getMessage());
                return;
            }
            limpiar();
            vista.btnHabilitar.setEnabled(false);
        }
        
        if(e.getSource() == vista.btnDeshabilitar) {
            ingresar();
            try {
                this.pro.setStatus(0);
                db.deshabilitar(this.pro);
                JOptionPane.showMessageDialog(vista, "Se logró deshabilitar");
                
                vista.txtCodigo.setEnabled(true);
                vista.txtNombre.setEnabled(false);
                vista.txtPrecio.setEnabled(false);
                vista.jDateChooser1.setEnabled(false);
                vista.btnGuardar.setEnabled(false);
                vista.btnDeshabilitar.setEnabled(false);
                limpiar();
            }
            catch(Exception ex) {
                JOptionPane.showMessageDialog(vista, "SURGIO UN ERROR: " + ex.getMessage());
                return;
            }
        }
        
        if(e.getSource() == vista.btnBuscar) {
            
            try {
                if("".equalsIgnoreCase(vista.txtCodigo.getText()))
                {
                    throw new IllegalArgumentException("Escribe un código");
                }
                if(Integer.parseInt(vista.txtCodigo.getText()) <= 0)
                    throw new IllegalArgumentException("No puede haber un código negativo o cero");
            }
            catch(NumberFormatException ex) {
                JOptionPane.showMessageDialog(vista, "SURGIO UN ERROR: " + ex.getMessage());
                return;
            }
            catch(IllegalArgumentException ex) {
                JOptionPane.showMessageDialog(vista, "SURGIO UN ERROR: " + ex.getMessage());
                return;
            }
            
            try {
                this.pro.setCode(vista.txtCodigo.getText());
                Object objPro = db.buscar(this.pro.getCode(), 2);
                Productos pro = (Productos) objPro;
                
                if((db.isExists(vista.txtCodigo.getText()) == true) && (pro.getStatus() == 1))
                {
                    JOptionPane.showMessageDialog(vista , "Se encontro el codigo, ahora puede editar sus datos o darlo de baja");
                    vista.txtCodigo.setEnabled(false);
                    vista.txtNombre.setEnabled(true);
                    vista.txtPrecio.setEnabled(true);
                    vista.jDateChooser1.setEnabled(true);
                    vista.btnGuardar.setEnabled(true);
                    vista.btnDeshabilitar.setEnabled(true);
                    sw = 2;
                }
                else if((db.isExists(vista.txtCodigo.getText()) == true) && (pro.getStatus() == 0))
                {
                    JOptionPane.showMessageDialog(vista , "Existe, pero se encuentra deshabilitado");
                    return;
                }
                else if(sw == 0)
                {
                    JOptionPane.showMessageDialog(vista, "Este codigo esta disponible");
                    return;
                }
                else
                {
                    JOptionPane.showMessageDialog(vista, "Este codigo esta disponible");
                    vista.btnHabilitar.setEnabled(false);
                    vista.txtCodigo.setText(this.pro.getCode());
                    vista.btnDeshabilitar.setEnabled(false);
                    vista.btnGuardar.setEnabled(true);
                    sw = 1;
                    return;
                }
                
                this.pro.setIdPro(pro.getIdPro());
                this.pro.setCode(pro.getCode());
                this.pro.setNombre(pro.getNombre());
                this.pro.setPre(pro.getPre());
                this.pro.setFecha(pro.getFecha());
                this.pro.setStatus(pro.getStatus());
                
                vista.txtID.setText(Integer.toString(this.pro.getIdPro()));
                vista.txtCodigo.setText(this.pro.getCode());
                vista.txtNombre.setText(this.pro.getNombre());
                vista.txtPrecio.setText(Float.toString(this.pro.getPre()));
                
                SimpleDateFormat dateFormat = new SimpleDateFormat("YYYY-MM-dd");
                
                Date date = dateFormat.parse(pro.getFecha());
                vista.jDateChooser1.setDate(date);
            }
            catch(Exception ex) {
                JOptionPane.showMessageDialog(vista, "Error: " + ex.getMessage());
                return;
            }
        }
        
        if(e.getSource() == vista.btnSearch2) {
            try {
                if(Integer.parseInt(vista.txtCode2.getText()) <= 0){
                    throw new IllegalArgumentException("No puede haber un código negativo o cero");
                }
                this.pro.setCode(vista.txtCode2.getText());
                Object objPro = db.buscar(this.pro.getCode(), 2);
                Productos pro = (Productos) objPro;
                
                if((db.isExists(vista.txtCode2.getText()) == true) && (pro.getStatus() == 0))
                    vista.btnHabilitar.setEnabled(true);
                else if(pro.getStatus() == 1)
                {
                    JOptionPane.showMessageDialog(vista, "Este codigo esta activo, ingrese otro");
                    vista.btnHabilitar.setEnabled(false);
                    return;
                }                  
                else
                {
                    JOptionPane.showMessageDialog(vista, "El codigo no existe");
                    vista.btnHabilitar.setEnabled(false);
                    return;
                }
                
                this.pro.setIdPro(pro.getIdPro());
                this.pro.setCode(pro.getCode());
                this.pro.setNombre(pro.getNombre());
                this.pro.setPre(pro.getPre());
                this.pro.setFecha(pro.getFecha());
                this.pro.setStatus(pro.getStatus());
                
                vista.txtCode2.setText(this.pro.getCode());
                vista.lname.setText(this.pro.getNombre());

            }
            catch(Exception ex) {
                JOptionPane.showMessageDialog(vista, "SURGIO UN ERROR: " + ex.getMessage());
                return;
            }
        }
        
        if(e.getSource() == vista.btnLimpiar)
            limpiar();
        
        if(e.getSource() == vista.btnCancelar)
        {
            limpiar();
            vista.txtCodigo.setEnabled(true);
            vista.txtNombre.setEnabled(false);
            vista.txtPrecio.setEnabled(false);
            vista.jDateChooser1.setEnabled(false);
            //vista.btnGuardar.setText("Insertar | Guardar");
            vista.btnGuardar.setEnabled(false);
            vista.btnDeshabilitar.setEnabled(false);
            vista.btnLimpiar.setEnabled(false);
            vista.btnCancelar.setEnabled(false);
        }
        
        if(e.getSource() == vista.btnCerrar)
        {
            int option = JOptionPane.showConfirmDialog(vista, "¿Deseas salir?", "Salida", JOptionPane.YES_NO_OPTION);
            if(option == JOptionPane.YES_OPTION)
            {
                vista.dispose();
                System.exit(0);
            }
        }
        
        ActualizarTabla(vista.jtbActivos, 1);
        ActualizarTabla(vista.jtbNoActivos, 0);
    }
    
    public static void main(String[] args) {
        Productos pro = new Productos();
        dbProducto db = new dbProducto();
        dlgProductos vista = new dlgProductos(new JFrame(), true);
        
        TestDb controlador = new TestDb(pro, db, vista);
        controlador.iniciarVista();
    }
    
}

